### WARNING

- **Do NOT remove `~/.Xauthority` when you are working normally on the GNU/Linux with X Window based desktop environment, otherwise the X Window Server will crash and GUI applications can no longer start until next login.** In particular, do NOT include the `~/.Xauthority` file in routine clean up.

- **Keep the `~/.config/qt5ct/qt5ct.conf` file existing permanently**, otherwise the GUI of applications including okular and Manjaro Settings Manager will be affected. The default `qt5ct.conf` for Manjaro XFCE (21.0.6) is demonstrated as follows.
```
[Appearance]
color_scheme_path=
custom_palette=false
icon_theme=Papirus-Maia
style=kvantum

[Fonts]
fixed=@Variant(\0\0\0@\0\0\0\x12\0\x43\0\x61\0n\0t\0\x61\0r\0\x65\0l\0l@$\0\0\0\0\0\0\xff\xff\xff\xff\x5\x1\0\x32\x10)
general=@Variant(\0\0\0@\0\0\0\x12\0\x43\0\x61\0n\0t\0\x61\0r\0\x65\0l\0l@$\0\0\0\0\0\0\xff\xff\xff\xff\x5\x1\0\x32\x10)

[Interface]
activate_item_on_single_click=1
buttonbox_layout=0
cursor_flash_time=1000
dialog_buttons_have_icons=1
double_click_interval=400
gui_effects=@Invalid()
menus_have_icons=true
stylesheets=@Invalid()

[SettingsWindow]
geometry=@ByteArray(\x1\xd9\xd0\xcb\0\x2\0\0\0\0\x1\x9a\0\0\x1\x1\0\0\x3\xd8\0\0\x3w\0\0\x1\x9b\0\0\x1\x19\0\0\x3\xd7\0\0\x3v\0\0\0\0\0\0\0\0\x5\0)
```

### General initializations for GNU/Linux

- Partial usages of `efibootmgr` command
```
sudo efibootmgr -C -d /dev/sdb -p 2 -b 0020 -l "/EFI/Microsoft/Boot/bootmgfw.efi" -L "Windows Boot Manager(0)"
sudo efibootmgr -C -d /dev/sdb -p 2 -b 0021 -l "/EFI/Microsoft/Boot/bootmgfw.efi" -L "Windows Boot Manager(1)"
sudo efibootmgr -C -d /dev/sdb -p 2 -b 0022 -l "/EFI/Microsoft/Boot/bootmgfw.efi" -L "Windows Boot Manager(2)"
sudo efibootmgr -C -d /dev/sdb -p 2 -b 0023 -l "/EFI/Microsoft/Boot/bootmgfw.efi" -L "Windows Boot Manager(3)"
sudo efibootmgr -C -d /dev/sdb -p 2 -b 0024 -l "/EFI/Microsoft/Boot/bootmgfw.efi" -L "Windows Boot Manager(4)"
sudo efibootmgr -C -d /dev/sdb -p 2 -b 0025 -l "/EFI/Microsoft/Boot/bootmgfw.efi" -L "Windows Boot Manager(5)"
sudo efibootmgr -C -d /dev/sdb -p 2 -b 0026 -l "/EFI/Microsoft/Boot/bootmgfw.efi" -L "Windows Boot Manager(6)"
sudo efibootmgr -C -d /dev/sdb -p 2 -b 0027 -l "/EFI/Microsoft/Boot/bootmgfw.efi" -L "Windows Boot Manager(7)"
sudo efibootmgr -C -d /dev/sdb -p 2 -b 0030 -l "\EFI\Manjaro-sda3-EXT4_01\grubx64.efi" -L Manjaro-sda3-EXT4_01
sudo efibootmgr -C -d /dev/sdb -p 2 -b 0031 -l "\EFI\Manjaro-sda3-EXT4_02\grubx64.efi" -L Manjaro-sda3-EXT4_02
```

- Alias definitions in `~/.bashrc`
```
# There are 3 different types of shells in bash: the login shell, normal shell
# and interactive shell. Login shells read ~/.profile and interactive shells
# read ~/.bashrc; in our setup, /etc/profile sources ~/.bashrc - thus all
# settings made here will also take effect in a login shell.
#
# NOTE: It is recommended to make language settings in ~/.profile rather than
# here, since multilingual X sessions would not work properly if LANG is over-
# ridden in every subshell.

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

test -s ~/.bash_aliases && . ~/.bash_aliases || true
test -s ~/.alias && . ~/.alias || true
```

- Sample `~/.bash_aliases` or `~/.alias`
```
function clear_JetBrainsIDE_recent_projects()
{
    # IntelliJ IDEA Ultimate
    xmlPath=~/.IntelliJIdea2018.3/config/options/recentProjects.xml
    echo '<application>' > $xmlPath                                          
    echo '  <component name="RecentProjectsManager">' >> $xmlPath
    echo '    <option name="pid" value="" />' >> $xmlPath
    echo '  </component>' >> $xmlPath
    echo '</application>' >> $xmlPath
    xmlPath=~/.IntelliJIdea2019.1/config/options/recentProjects.xml
    echo '<application>' > $xmlPath                                          
    echo '  <component name="RecentProjectsManager">' >> $xmlPath
    echo '    <option name="pid" value="" />' >> $xmlPath
    echo '  </component>' >> $xmlPath
    echo '</application>' >> $xmlPath
    xmlPath=~/.IntelliJIdea2019.3/config/options/recentProjects.xml
    echo '<application>' > $xmlPath                                          
    echo '  <component name="RecentProjectsManager">' >> $xmlPath
    echo '    <option name="pid" value="" />' >> $xmlPath
    echo '  </component>' >> $xmlPath
    echo '</application>' >> $xmlPath
    xmlPath=~/.config/JetBrains/IntelliJIdea2021.1/options/recentProjects.xml
    echo '<application>' > $xmlPath                                          
    echo '  <component name="RecentProjectsManager">' >> $xmlPath
    echo '    <option name="pid" value="" />' >> $xmlPath
    echo '  </component>' >> $xmlPath
    echo '</application>' >> $xmlPath
    rm $xmlPath
    # PyCharm Professional
    xmlPath=~/.PyCharm2019.1/config/options/recentProjectDirectories.xml
    echo '<application>' > $xmlPath
    echo '  <component name="RecentDirectoryProjectsManager">' >> $xmlPath
    echo '    <option name="pid" value="" />' >> $xmlPath
    echo '  </component>' >> $xmlPath
    echo '</application>' >> $xmlPath
    xmlPath=~/.config/JetBrains/PyCharm2021.1/options/recentProjectDirectories.xml
    echo '<application>' > $xmlPath
    echo '  <component name="RecentDirectoryProjectsManager">' >> $xmlPath
    echo '    <option name="pid" value="" />' >> $xmlPath
    echo '  </component>' >> $xmlPath
    echo '</application>' >> $xmlPath
    rm $xmlPath
    # PyCharm Community
    xmlPath=~/.PyCharmCE2019.1/config/options/recentProjectDirectories.xml
    echo '<application>' > $xmlPath
    echo '  <component name="RecentDirectoryProjectsManager">' >> $xmlPath
    echo '    <option name="pid" value="" />' >> $xmlPath
    echo '  </component>' >> $xmlPath
    echo '</application>' >> $xmlPath
    # CLion
    xmlPath=~/.CLion2019.1/config/options/recentProjectDirectories.xml
    echo '<application>' > $xmlPath
    echo '  <component name="RecentDirectoryProjectsManager">' >> $xmlPath
    echo '    <option name="pid" value="" />' >> $xmlPath
    echo '  </component>' >> $xmlPath
    echo '</application>' >> $xmlPath
    xmlPath=~/.CLion2019.2/config/options/recentProjectDirectories.xml
    echo '<application>' > $xmlPath
    echo '  <component name="RecentDirectoryProjectsManager">' >> $xmlPath
    echo '    <option name="pid" value="" />' >> $xmlPath
    echo '  </component>' >> $xmlPath
    echo '</application>' >> $xmlPath
    xmlPath=~/.CLion2019.3/config/options/recentProjectDirectories.xml
    echo '<application>' > $xmlPath
    echo '  <component name="RecentDirectoryProjectsManager">' >> $xmlPath
    echo '    <option name="pid" value="" />' >> $xmlPath
    echo '  </component>' >> $xmlPath
    echo '</application>' >> $xmlPath
    # GoLand
    xmlPath=~/.GoLand2017.3/config/options/recentProjectDirectories.xml
    echo '<application>' > $xmlPath
    echo '  <component name="RecentDirectoryProjectsManager">' >> $xmlPath
    echo '    <option name="pid" value="" />' >> $xmlPath
    echo '  </component>' >> $xmlPath
    echo '</application>' >> $xmlPath
    xmlPath=~/.GoLand2018.1/config/options/recentProjectDirectories.xml
    echo '<application>' > $xmlPath
    echo '  <component name="RecentDirectoryProjectsManager">' >> $xmlPath
    echo '    <option name="pid" value="" />' >> $xmlPath
    echo '  </component>' >> $xmlPath
    echo '</application>' >> $xmlPath
    xmlPath=~/.GoLand2018.2/config/options/recentProjectDirectories.xml
    echo '<application>' > $xmlPath
    echo '  <component name="RecentDirectoryProjectsManager">' >> $xmlPath
    echo '    <option name="pid" value="" />' >> $xmlPath
    echo '  </component>' >> $xmlPath
    echo '</application>' >> $xmlPath
    xmlPath=~/.GoLand2018.3/config/options/recentProjectDirectories.xml
    echo '<application>' > $xmlPath
    echo '  <component name="RecentDirectoryProjectsManager">' >> $xmlPath
    echo '    <option name="pid" value="" />' >> $xmlPath
    echo '  </component>' >> $xmlPath
    echo '</application>' >> $xmlPath
    xmlPath=~/.GoLand2019.1/config/options/recentProjectDirectories.xml
    echo '<application>' > $xmlPath
    echo '  <component name="RecentDirectoryProjectsManager">' >> $xmlPath
    echo '    <option name="pid" value="" />' >> $xmlPath
    echo '  </component>' >> $xmlPath
    echo '</application>' >> $xmlPath
    xmlPath=~/.GoLand2019.2/config/options/recentProjectDirectories.xml
    echo '<application>' > $xmlPath
    echo '  <component name="RecentDirectoryProjectsManager">' >> $xmlPath
    echo '    <option name="pid" value="" />' >> $xmlPath
    echo '  </component>' >> $xmlPath
    echo '</application>' >> $xmlPath
}

alias clion1901="/opt/JetBrains/clion-2019.1.4/bin/clion.sh"
alias clion1902="/opt/JetBrains/clion-2019.2.5/bin/clion.sh"
alias clion1903="/opt/JetBrains/clion-2019.3.6/bin/clion.sh"

# alias idea1803="/opt/JetBrains/idea-IU-183.6156.11/bin/idea.sh"
# alias idea1901="/opt/JetBrains/idea-IU-191.8026.42/bin/idea.sh"
# alias idea1902="/opt/JetBrains/idea-IU-192.7142.36/bin/idea.sh"
alias idea1903="/opt/JetBrains/idea-IU-193.7288.26/bin/idea.sh"
alias idea2101="/opt/JetBrains/idea-IU-211.7628.21/bin/idea.sh"

# alias pycharmc1901='/opt/JetBrains/pycharm-community-anaconda-2019.1.4/bin/pycharm.sh'
# alias pycharmc1902='/opt/JetBrains/pycharm-community-anaconda-2019.2.6/bin/pycharm.sh'

alias pycharm1901='/opt/JetBrains/pycharm-anaconda-2019.1.4/bin/pycharm.sh'
alias pycharm2101='/opt/JetBrains/pycharm-2021.1.3/bin/pycharm.sh'

alias cls='history -c; \
            rm -rf ~/.cache/ ~/.local/ ~/.java/fonts/ ~/.android/ \
                    ~/.gnupg/ ~/.nv/ ~/.dbus/ ~/.sudo_as_admin_successful; \
            rm -rf ~/.ssh/known_hosts*; \
            rm -rf ~/.ssh/known_hosts; rm -rf ~/.ssh/known_hosts.old; \
            rm -rf ~/.xsession-errors*; \
            rm -rf ~/.xsession-errors; rm -rf ~/.xsession-errors.old; \
            rm -rf ~/.config/okular*; \
            rm -rf ~/.bash_history; history -c; \
            rm -rf ~/.npm/ && npm cache clean --force; \
            clear_JetBrainsIDE_recent_projects; reset'
cls

alias exit='cls && exit'
alias poweroff='cls && poweroff'
alias reboot='cls && reboot'

# The following alias is only for `pacman` package manager
# commonly used in ArchLinux and Manjaro Linux, etc.
alias pacclean='sudo pacman -Runs --confirm  $(sudo pacman -Qdtq)'
```


### Common initializations for Manjaro Linux

- `/etc/pacman.d/mirrorlist`
```
##
## Manjaro Linux default mirrorlist
## Generated on 2021-04-25 00:00
##
## Please use 'pacman-mirrors -f [NUMBER] [NUMBER]' to modify mirrorlist
## (Use 0 for all mirrors)
##

## Country : China
Server = https://mirrors.aliyun.com/manjaro/stable/$repo/$arch

## Country : China
Server = https://mirrors.zju.edu.cn/manjaro/stable/$repo/$arch

## Country : China
Server = https://mirrors.ustc.edu.cn/manjaro/stable/$repo/$arch

## Country : China
Server = https://mirrors.bfsu.edu.cn/manjaro/stable/$repo/$arch

## Country : China
Server = https://mirrors.sjtug.sjtu.edu.cn/manjaro/stable/$repo/$arch

## Country : China
Server = https://mirrors.tuna.tsinghua.edu.cn/manjaro/stable/$repo/$arch

## Country : China
Server = https://mirrors.pku.edu.cn/manjaro/stable/$repo/$arch

## Country : United_States
Server = https://mirror.math.princeton.edu/pub/manjaro/stable/$repo/$arch

## Country : Canada
Server = https://osmirror.org/manjaro/stable/$repo/$arch

## Country : United_Kingdom
Server = http://repo.manjaro.org.uk/stable/$repo/$arch

## Country : United_Kingdom
Server = http://mirror.catn.com/pub/manjaro/stable/$repo/$arch

## Country : France
Server = https://manjaro.ynh.ovh/stable/$repo/$arch

## Country : France
Server = https://mirror.oldsql.cc/manjaro/stable/$repo/$arch

## Country : Germany
Server = http://mirror.ragenetwork.de/manjaro/stable/$repo/$arch

## Country : Belarus
Server = http://mirror.datacenter.by/pub/mirrors/manjaro/stable/$repo/$arch

## Country : Brazil
Server = http://linorg.usp.br/manjaro/stable/$repo/$arch

## Country : Portugal
Server = http://ftp.dei.uc.pt/pub/linux/manjaro/stable/$repo/$arch

## Country : Belarus
Server = http://mirror.datacenter.by/pub/mirrors/manjaro/stable/$repo/$arch

## Country : Ecuador
Server = https://mirror.cedia.org.ec/manjaro/stable/$repo/$arch

## Country : Taiwan
Server = http://free.nchc.org.tw/manjaro/stable/$repo/$arch

## Country : Australia
Server = http://manjaro.melbourneitmirror.net/stable/$repo/$arch

## Country : Spain
Server = https://ftp.caliu.cat/pub/distribucions/manjaro/stable/$repo/$arch

## Country : Brazil
Server = https://www.caco.ic.unicamp.br/manjaro/stable/$repo/$arch

## Country : United_Kingdom
Server = http://mirror.catn.com/pub/manjaro/stable/$repo/$arch

## Country : Bulgaria
Server = http://manjaro.telecoms.bg/stable/$repo/$arch

## Country : Germany
Server = https://mirror.netzspielplatz.de/manjaro/packages/stable/$repo/$arch

## Country : Italy
Server = https://ba.mirror.garr.it/mirrors/manjaro/stable/$repo/$arch

## Country : Kenya
Server = https://manjaro.mirror.liquidtelecom.com/stable/$repo/$arch
```

- `/etc/pacman.conf`
```
#
# /etc/pacman.conf
#
# See the pacman.conf(5) manpage for option and repository directives

#
# GENERAL OPTIONS
#
[options]
# The following paths are commented out with their default values listed.
# If you wish to use different paths, uncomment and update the paths.
#RootDir     = /
#DBPath      = /var/lib/pacman/
CacheDir = /var/cache/pacman/pkg/
#LogFile     = /var/log/pacman.log
#GPGDir      = /etc/pacman.d/gnupg/
#HookDir     = /etc/pacman.d/hooks/
HoldPkg      = pacman glibc manjaro-system
# If upgrades are available for these packages they will be asked for first
SyncFirst    = manjaro-system archlinux-keyring manjaro-keyring
#XferCommand = /usr/bin/curl -C - -f %u > %o
#XferCommand = /usr/bin/wget --passive-ftp -c -O %o %u
#CleanMethod = KeepInstalled
#UseDelta    = 0.7
Architecture = auto

# Pacman won't upgrade packages listed in IgnorePkg and members of IgnoreGroup
#IgnorePkg   =
#IgnoreGroup =

#NoUpgrade   =
#NoExtract   =

# Misc options
#UseSyslog
#Color
#TotalDownload
# We cannot check disk space from within a chroot environment
CheckSpace
#VerbosePkgLists

# By default, pacman accepts packages signed by keys that its local keyring
# trusts (see pacman-key and its man page), as well as unsigned packages.
SigLevel    = Required DatabaseOptional
LocalFileSigLevel = Optional
#RemoteFileSigLevel = Required

# NOTE: You must run `pacman-key --init` before first using pacman; the local
# keyring can then be populated with the keys of all official Manjaro Linux
# packagers with `pacman-key --populate archlinux manjaro`.

#
# REPOSITORIES
#   - can be defined here or included from another file
#   - pacman will search repositories in the order defined here
#   - local/custom mirrors can be added here or in separate files
#   - repositories listed first will take precedence when packages
#     have identical names, regardless of version number
#   - URLs will have $repo replaced by the name of the current repo
#   - URLs will have $arch replaced by the name of the architecture
#
# Repository entries are of the format:
#       [repo-name]
#       Server = ServerName
#       Include = IncludePath
#
# The header [repo-name] is crucial - it must be present and
# uncommented to enable the repo.
#

# The testing repositories are disabled by default. To enable, uncomment the
# repo name header and Include lines. You can add preferred servers immediately
# after the header, and they will be used before the default mirrors.

[core]
SigLevel = PackageRequired
Include = /etc/pacman.d/mirrorlist

[extra]
SigLevel = PackageRequired
Include = /etc/pacman.d/mirrorlist

[community]
SigLevel = PackageRequired
Include = /etc/pacman.d/mirrorlist

# If you want to run 32 bit applications on your x86_64 system,
# enable the multilib repositories as required here.

[multilib]
SigLevel = PackageRequired
Include = /etc/pacman.d/mirrorlist

# An example of a custom package repository.  See the pacman manpage for
# tips on creating your own repositories.
#[custom]
#SigLevel = Optional TrustAll
#Server = file:///home/custompkgs

[archlinuxcn]
SigLevel = Optional TrustAll
Server = https://mirrors.tuna.tsinghua.edu.cn/archlinuxcn/$arch
Server = https://mirrors.sjtug.sjtu.edu.cn/archlinux-cn/$arch
Server = https://mirrors.ustc.edu.cn/archlinuxcn/$arch

[arch4edu]
SigLevel = Optional TrustAll
Server = https://mirrors.tuna.tsinghua.edu.cn/arch4edu/$arch
```

 - 开源字体
[https://wiki.archlinux.org/index.php/Localization/Simplified_Chinese_(简体中文)](https://wiki.archlinux.org/index.php/Localization/Simplified_Chinese_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87))

- fcitx输入法
<https://wiki.archlinux.org/index.php/Fcitx>
[https://wiki.archlinux.org/index.php/Fcitx_(简体中文)](https://wiki.archlinux.org/index.php/Fcitx_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87))
将以下配置加入`.pam_environment`
```
GTK_IM_MODULE DEFAULT=fcitx
QT_IM_MODULE  DEFAULT=fcitx
XMODIFIERS    DEFAULT=\@im=fcitx
```
或将以下配置加入`~/.xprofile`
```
export GTK_IM_MODULE=fcitx
export QT_IM_MODULE=fcitx
export XMODIFIERS=@im=fcitx
# or following
# export XMODIFIERS="@im=fcitx"
```

- Installing legacy packages or Downgrading packages
    - <https://wiki.manjaro.org/index.php/Downgrading_packages>
    - <https://wiki.archlinux.org/index.php/Arch_Linux_Archive>
        - <https://archive.archlinux.org/packages/c/cuda/>

- Network Manager
[https://wiki.archlinux.org/index.php/NetworkManager_(简体中文)](https://wiki.archlinux.org/index.php/NetworkManager_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87))

- GRUB引导修复
<https://wiki.manjaro.org/index.php/Restore_the_GRUB_Bootloader>

- shadowsocks with obfs support
<https://www.solarck.com/shadowsocks-libev.html>